const http = require('http');
const uuid = require('uuid')
const format = require('date-fns/format')
const pkg = require('./package.json')

const port = process.env.PORT || 3000;

const reply = {
  date: format(new Date()),
  uuid: uuid.v4(),
  version: pkg.version
}

http.createServer((req, res) => {
  console.log(`${req.method} - ${req.url}`)
  res.writeHead(200, {
    'content-type': 'application/json'
  })

  res.end(JSON.stringify(reply))
}).listen(port);

