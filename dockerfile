FROM mhart/alpine-node:6

ENV NODE_ENV=production

RUN npm install -g yarn

WORKDIR /usr/local/app
COPY package.json yarn.lock ./

RUN yarn --pure-lockfile
COPY . .

EXPOSE 3000

CMD ["yarnpkg", "run", "start"]
