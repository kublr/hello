if [ -z "$1" ]; then
  echo "No SHA1 provided"
  exit 1
fi

SHA1=$1

cat << EOF
apiVersion: v1
kind: Service
metadata:
  name: hello
  labels:
    name: hello
spec:
  ports:
  - port: 80
    targetPort: 3000
    protocol: TCP
  type: LoadBalancer
  selector:
    name: hello

---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: hello
spec:
  replicas: 3
  template:
    metadata:
      labels:
        name: hello
    spec:
      containers:
      - name: hello
        image: registry.gitlab.com/kublr/hello:$SHA1
        ports:
        - containerPort: 3000
      imagePullSecrets:
      - name: gitlab-registry
EOF
